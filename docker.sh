#bin/bash
containers=$(docker ps -a -q --filter="name=website")
 if [ ! -z $containers ]; then
  docker stop $containers;
  docker rm $containers;
 fi